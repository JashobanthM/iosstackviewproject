//
//  UserDetails.swift
//  iosStackViewProject
//
//  Created by Jashobanth Mohanty on 5/29/20.
//  Copyright © 2020 Jashobanth Mohanty. All rights reserved.
//

import Foundation

enum Badge: String, Codable {
    case gold
    case silver
    case bronze
}

public struct Users: Codable {
    let users: [UserDetails]
    
    enum CodingKeys: String, CodingKey {
        case users = "items"
    }
}

public struct UserDetails: Codable {
    let username: String
    let avatarURL: String
    let badgeCounts: BadgeCount?
    var badges: [Badge]?
    
    enum CodingKeys: String, CodingKey {
           case username = "display_name"
           case avatarURL = "profile_image"
           case badgeCounts = "badge_counts"
           case badges
       }
}

struct BadgeCount: Codable {
    let gold: Int?
    let bronze: Int?
    let silver: Int?
}
