//
//  UINavigationBar+Extension.swift
//  iosStackViewProject
//
//  Created by Jashobanth Mohanty on 5/30/20.
//  Copyright © 2020 Jashobanth Mohanty. All rights reserved.
//

import UIKit

extension UINavigationController {
    func styleNavigationBarTitle() {
        navigationBar.barTintColor = Factory.colors.navigationBarColor
        navigationBar.layoutMargins.left = 25
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black, NSAttributedString.Key.font:UIFont.systemFont(ofSize: 17, weight: .semibold)]
        navigationBar.titleTextAttributes = textAttributes
        navigationBar.tintColor = .white
    }
}
