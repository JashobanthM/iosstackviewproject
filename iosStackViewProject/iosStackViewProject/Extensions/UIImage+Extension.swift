//
//  UIImage+Extension.swift
//  iosStackViewProject
//
//  Created by Jashobanth Mohanty on 5/31/20.
//  Copyright © 2020 Jashobanth Mohanty. All rights reserved.
//

import UIKit

extension UIImage {
    func resizeImage(dimension: CGFloat,
                     opaque: Bool) -> UIImage {
        var width: CGFloat
        var height: CGFloat
        var newImage: UIImage
        
        let size = self.size
        let aspectRatio =  size.width/size.height
        
        height = dimension
        width = dimension * aspectRatio
        let renderFormat = UIGraphicsImageRendererFormat.default()
        renderFormat.opaque = opaque
        let renderer = UIGraphicsImageRenderer(size: CGSize(width: width,
                                                            height: height),
                                               format: renderFormat)
        newImage = renderer.image {
            (context) in
            self.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
        }
        return newImage
    }
}
