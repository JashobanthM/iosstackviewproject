//
//  UIStackView+Extension.swift
//  iosStackViewProject
//
//  Created by Jashobanth Mohanty on 5/31/20.
//  Copyright © 2020 Jashobanth Mohanty. All rights reserved.
//

import UIKit

public extension UIStackView {
    func addArrangedSubviews(_ views: UIView...) {
        views.forEach { addArrangedSubview($0) }
    }
}
