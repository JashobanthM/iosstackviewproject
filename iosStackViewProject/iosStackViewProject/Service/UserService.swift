//
//  UserService.swift
//  iosStackViewProject
//
//  Created by Jashobanth Mohanty on 5/29/20.
//  Copyright © 2020 Jashobanth Mohanty. All rights reserved.
//

import Foundation

public protocol UserServiceable {
    func getusers(completionHandler: @escaping (Result<[UserDetails], NetworkError>) -> Void)
}

public class UserService: UserServiceable {
    private let networking: Networkable

    public init(networking: Networkable) {
        self.networking = networking
    }

    public convenience init() {
        self.init(networking: Networking())
    }
    
    public func getusers(completionHandler: @escaping (Result<[UserDetails], NetworkError>) -> Void) {
        let decoder = JSONDecoder()
        let endpoint: EndPoint = UsersEndpoint.users
        let site = "stackoverflow"
        
        guard let request = endpoint.buildUrlRequest(queryItems: [URLQueryItem(name: "site",
                                                                               value: site)])
            else {
                completionHandler(.failure(.badUrl))
                return
        }
        networking.request(request: request) { (data, response, error) in
            if let error = error {
                completionHandler(.failure(error))
            }

            if let data = data {
                if let response = response as? HTTPURLResponse  {
                    if response.statusCode != 200 {
                        completionHandler(.failure(.badUrl))
                    }
                }
                if let usersList = try? decoder.decode(Users.self, from: data) {
                    completionHandler(.success(usersList.users))
                }
            } else {
                completionHandler(.failure(.badData))
            }
        }
    }
}
