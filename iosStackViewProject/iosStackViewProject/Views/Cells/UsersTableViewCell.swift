//
//  UsersTableViewCell.swift
//  iosStackViewProject
//
//  Created by Jashobanth Mohanty on 5/30/20.
//  Copyright © 2020 Jashobanth Mohanty. All rights reserved.
//

import SnapKit
import UIKit

class UsersTableViewCell: UITableViewCell {
    
    private let verticalMargin: CGFloat = 16.0
    private let horizontalMargin: CGFloat = 36
    
    lazy var view = UIView()
    lazy var header: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        label.numberOfLines = 1
        label.textColor = .black
        label.backgroundColor = .clear
        return label
    }()
    
    lazy var goldBadge: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        label.numberOfLines = 0
        label.text = "Gold"
        return label
    }()
    
    lazy var goldBadgeCount: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.numberOfLines = 0
        return label
    }()
    
    lazy var silverBadge: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        label.numberOfLines = 0
        label.text = "Silver"
        return label
    }()
    
    lazy var silverBadgeCount: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.numberOfLines = 0
        return label
    }()
    
    lazy var bronzeBadge: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        label.numberOfLines = 0
        label.text = "Bronze"
        return label
    }()
    
    lazy var bronzeBadgCount: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.numberOfLines = 0
        
        return label
    }()
    
    lazy var horizontalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 5
        return stackView
    }()
    
    lazy var thumbnail: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = false
        return imageView
    }()
    
    lazy var goldVerticalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 5
        return stackView
    }()
    
    lazy var silverVerticalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 5
        return stackView
    }()
    
    lazy var bronzeVerticalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 5
        return stackView
    }()
    
    lazy var thumbnailWrapper: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 5;
        view.layer.masksToBounds = true;
        view.backgroundColor = .lightGray
        return view
    }()
    
    lazy var verticalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 5
        return stackView
    }()
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        layoutView()
        self.contentView.backgroundColor = Factory.colors.appBackgroundColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func configure(user: UserDetails) {
        header.text = user.username
        goldVerticalStackView.isHidden = user.badgeCounts?.gold ?? 0 == 0
        silverVerticalStackView.isHidden = user.badgeCounts?.silver ?? 0 == 0
        bronzeVerticalStackView.isHidden = user.badgeCounts?.bronze ?? 0 == 0
        goldBadgeCount.text = String(user.badgeCounts?.gold ?? 0)
        silverBadgeCount.text =  String(user.badgeCounts?.silver ?? 0)
        bronzeBadgCount.text = String(user.badgeCounts?.bronze ?? 0) 
    }
    
    private func layoutView() {
        goldVerticalStackView.addArrangedSubviews(goldBadge,goldBadgeCount)
        silverVerticalStackView.addArrangedSubviews(silverBadge,silverBadgeCount)
        bronzeVerticalStackView.addArrangedSubviews(bronzeBadge,bronzeBadgCount)
        horizontalStackView.addArrangedSubviews(goldVerticalStackView,
                                                silverVerticalStackView,
                                                bronzeVerticalStackView)
        thumbnailWrapper.addSubview(thumbnail)
        thumbnail.center = CGPoint(x: thumbnailWrapper.bounds.size.width / 2,
                                   y: thumbnailWrapper.bounds.size.height / 2)
        
        verticalStackView.addArrangedSubview(header)
        verticalStackView.addArrangedSubview(horizontalStackView)
        
        view.addSubview(thumbnailWrapper)
        view.addSubview(verticalStackView)
        contentView.addSubview(view)
        
        view.snp.remakeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().inset(horizontalMargin)
            make.top.equalToSuperview().offset(verticalMargin)
            make.bottom.equalToSuperview().inset(verticalMargin)
        }
        
        header.snp.remakeConstraints { make in
            make.leading.top.equalToSuperview()
        }
        
        goldVerticalStackView.snp.makeConstraints { (make) in
            make.width.greaterThanOrEqualTo(45)
        }
        
        silverVerticalStackView.snp.makeConstraints { (make) in
            make.width.greaterThanOrEqualTo(45)
        }
        
        bronzeVerticalStackView.snp.makeConstraints { (make) in
            make.width.greaterThanOrEqualTo(45)
        }
        
        horizontalStackView.snp.remakeConstraints { make in
            make.leading.equalToSuperview()
        }
        
        thumbnailWrapper.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(5)
            make.leading.equalToSuperview().offset(5)
            make.height.equalTo(80.0)
            make.width.equalTo(80.0)
            make.bottom.equalToSuperview().inset(5)
        }
        
        thumbnail.snp.remakeConstraints { make in
            make.top.leading.equalToSuperview()
        }
        
        verticalStackView.snp.remakeConstraints { make in
            make.leading.equalTo(thumbnailWrapper.snp.trailing).offset(7)
            make.top.bottom.trailing.equalToSuperview()
        }
    }
}
