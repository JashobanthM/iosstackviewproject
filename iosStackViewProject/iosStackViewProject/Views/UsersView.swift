//
//  UsersView.swift
//  iosStackViewProject
//
//  Created by Jashobanth Mohanty on 5/30/20.
//  Copyright © 2020 Jashobanth Mohanty. All rights reserved.
//

import SnapKit
import UIKit

class UsersView: UIView {
    public var tableView = UITableView()
    public init() {
        super.init(frame: CGRect.zero)
        setupViews()
    }
    
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        self.backgroundColor = Factory.colors.appBackgroundColor
        setupTableView()
    }
    
    private func setupTableView() {
        addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.top.leading.trailing.bottom.equalToSuperview()
        }
        tableView.rowHeight = UITableView.automaticDimension
    }
}
