//
//  Colors.swift
//  iosStackViewProject
//
//  Created by Jashobanth Mohanty on 5/30/20.
//  Copyright © 2020 Jashobanth Mohanty. All rights reserved.
//

import UIKit

public struct Colors {
    public let navigationBarColor = UIColor(red: 46/255,
                                            green: 139/255,
                                            blue: 87/255,
                                            alpha: 0.8)
    public let appBackgroundColor = UIColor(red: 249/255,
                                            green: 249/255,
                                            blue: 249/255,
                                            alpha: 0.7)
}
