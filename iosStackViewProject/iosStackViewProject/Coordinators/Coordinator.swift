//
//  Coordinator.swift
//  iosStackViewProject
//
//  Created by Jashobanth Mohanty on 5/30/20.
//  Copyright © 2020 Jashobanth Mohanty. All rights reserved.
//

import UIKit
import Foundation

protocol Coordinator: AnyObject {
    var childCoordinators: [Coordinator] { get set }
    func start()
}

extension Coordinator {
    public func addChildCoordinator(_ coordinator: Coordinator) {
        childCoordinators.append(coordinator)
    }

    public func removeChildCoordinator(_ coordinator: Coordinator) {
        childCoordinators = childCoordinators.filter { $0 !== coordinator }
    }

    public func startChildCoordinator(_ childCoordinator: Coordinator) {
        childCoordinators.removeAll()
        addChildCoordinator(childCoordinator)
        childCoordinator.start()
    }
}
