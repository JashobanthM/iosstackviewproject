//
//  HomeCoordinator.swift
//  iosStackViewProject
//
//  Created by Jashobanth Mohanty on 5/30/20.
//  Copyright © 2020 Jashobanth Mohanty. All rights reserved.
//

import UIKit
import Foundation

class HomeCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    let router: Routable
    
    init(router: Routable) {
        self.router = router
    }
    
    func start() {
        let userViewController = UsersViewController()
        router.push(viewController: userViewController, animated: false)
    }
}
