//
//  UserTableViewDataManagers.swift
//  iosStackViewProject
//
//  Created by Jashobanth Mohanty on 5/30/20.
//  Copyright © 2020 Jashobanth Mohanty. All rights reserved.
//

import UIKit
import Foundation

class UserTableViewDataManager: NSObject, UITableViewDelegate, UITableViewDataSource {
    var users: [UserDetails] = []
    let networking: Networkable
    let imageCache = NSCache<NSString, UIImage>()
    
    private weak var tableView: UITableView?
    
    public init(networking: Networkable) {
        self.networking = networking
        super.init()
    }
    
    public convenience override init() {
        self.init(networking: Networking())
    }
    
    public func setup(tableView: UITableView, editingStyle: UITableViewCell.EditingStyle = .none) {
        self.tableView = tableView
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(UsersTableViewCell.self, forCellReuseIdentifier: String(describing: UsersTableViewCell.self))
        tableView.allowsSelection = false
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    public func setUsers(users: [UserDetails]) {
        self.users = users
        DispatchQueue.main.async {
            self.tableView?.reloadData()
        }
    }
    
    public func clearResults() {
        users = []
        DispatchQueue.main.async {
            self.tableView?.reloadData()
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView
            .dequeueReusableCell(withIdentifier: String(describing: UsersTableViewCell.self),
                                 for: indexPath) as? UsersTableViewCell
            else { return UsersTableViewCell() }
        
        let user = users[indexPath.row]
        cell.configure(user: user)
        cell.thumbnail.image = UIImage(named: "loadingIndicator")
        
        if let cachedImage = imageCache.object(forKey: user.username as NSString) {
            cell.thumbnail.image = cachedImage
        } else {
            DispatchQueue.global(qos: .background).async {
                if let url = URL(string:(user.avatarURL)) {
                    let data = try? Data(contentsOf: url)
                    DispatchQueue.main.async {
                        if let data = data,
                            var image: UIImage = UIImage(data: data) {
                            image = image.resizeImage(dimension: 80, opaque: false)
                            self.imageCache.setObject(image, forKey: NSString(string: (user.username)))
                            cell.thumbnail.image = image
                        } else {
                            cell.thumbnail.image = UIImage(named: "default")
                        }
                    }
                }
            }
        }
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
