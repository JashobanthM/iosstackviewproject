//
//  UserViewModel.swift
//  iosStackViewProject
//
//  Created by Jashobanth Mohanty on 5/29/20.
//  Copyright © 2020 Jashobanth Mohanty. All rights reserved.
//

import Foundation
public protocol UserVieweable {
    func getusers(completionHandler: @escaping (Result<[UserDetails], NetworkError>) -> Void)
}

public class UserViewModel: UserVieweable {
    let userService: UserServiceable
    
    init(userService: UserServiceable) {
        self.userService = userService
    }
    
    public convenience init() {
        self.init(userService: UserService())
    }
    
    public func getusers(completionHandler: @escaping (Result<[UserDetails], NetworkError>) -> Void) {
        userService.getusers(completionHandler: completionHandler)
    }
}
