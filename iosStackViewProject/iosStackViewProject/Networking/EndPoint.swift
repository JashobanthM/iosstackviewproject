//
//  EndPoint.swift
//  iosStackViewProject
//
//  Created by Jashobanth Mohanty on 5/29/20.
//  Copyright © 2020 Jashobanth Mohanty. All rights reserved.
//

import Foundation
public enum HttpMethod: String {
    case GET
}

public protocol EndPoint {
    var method: HttpMethod { get }
    var baseUrl: String { get }
    var path: String { get }

    func buildUrl(queryItems: [URLQueryItem]?) -> URL?
}

public extension EndPoint {
    func buildUrlRequest(queryItems: [URLQueryItem]? = nil) -> URLRequest? {
        guard let url = buildUrl(queryItems: queryItems) else { return nil }
        var request = URLRequest(url: url)
        request.httpMethod = self.method.rawValue
        return request
    }

    func buildUrl(queryItems: [URLQueryItem]?) -> URL? {
        let url = URL(string: self.baseUrl + self.path)
        if let queryItems = queryItems, url != nil {
            var urlComps = URLComponents(url: url!, resolvingAgainstBaseURL: false)
            urlComps?.queryItems = queryItems
            return urlComps?.url
        }
        return url
    }
}
