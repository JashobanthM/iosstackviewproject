//
//  UsersEndpoint.swift
//  iosStackViewProject
//
//  Created by Jashobanth Mohanty on 5/29/20.
//  Copyright © 2020 Jashobanth Mohanty. All rights reserved.
//

import Foundation
enum UsersEndpoint: EndPoint {
    
    case users

    var baseUrl: String {
        return "https://api.stackexchange.com"
    }

    var method: HttpMethod {
        switch self {
        case .users:
            return .GET
        }
    }

    var path: String {
        switch self {
        case .users:
            return "/2.2/users"
        }
    }
}
