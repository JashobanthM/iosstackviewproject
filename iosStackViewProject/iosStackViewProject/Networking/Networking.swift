//
//  Networking.swift
//  iosStackViewProject
//
//  Created by Jashobanth Mohanty on 5/29/20.
//  Copyright © 2020 Jashobanth Mohanty. All rights reserved.
//

import Foundation

public enum NetworkError: Error {
    case badUrl
    case badData
}

public protocol Networkable {
    func request(request: URLRequest?,
                 completionHandler: @escaping (Data?, URLResponse?, NetworkError?) -> Void)
}

public class Networking: Networkable {

    public func request(request: URLRequest?,
                        completionHandler: @escaping (Data?, URLResponse?, NetworkError?) -> Void) {
        let urlSession = URLSession.shared
        guard let request = request else {
            completionHandler(nil,nil,.badUrl)
            return
        }
        let task = urlSession.dataTask(with: request) { data,response,error in
            completionHandler(data,response,error as? NetworkError)
        }
        task.resume()
    }
}
