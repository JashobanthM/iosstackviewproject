//
//  UsersViewController.swift
//  iosStackViewProject
//
//  Created by Jashobanth Mohanty on 5/30/20.
//  Copyright © 2020 Jashobanth Mohanty. All rights reserved.
//

import Foundation
import UIKit

class UsersViewController: UIViewController {
    let usersView = UsersView()
    let usersDataManager: UserTableViewDataManager
    
    private var usersViewModel: UserVieweable?
    
    init(userViewModel: UserVieweable) {
        self.usersViewModel = userViewModel
        self.usersDataManager = UserTableViewDataManager()
        super.init(nibName: nil, bundle: nil)
    }
    
    public convenience init() {
          self.init(userViewModel: UserViewModel())
      }

      required init?(coder aDecoder: NSCoder) {
          fatalError("init(coder:) has not been implemented")
      }

   // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        self.usersDataManager.setup(tableView: usersView.tableView)
        fetchData()
    }
     // MARK: - Private

    private func setupView() {
        setupNavigationBar()
        self.view.backgroundColor = Factory.colors.appBackgroundColor
        view.addSubview(usersView)
        usersView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(64)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }

    private func setupNavigationBar() {
        title = "Home"
        self.navigationController?.styleNavigationBarTitle()
    }

    private func fetchData() {
        usersViewModel?.getusers(completionHandler: { (result) in
            switch result {
            case .success(let users):
                self.usersDataManager.setUsers(users: users)
            case .failure(let error):
                print(error.localizedDescription)
            }
        })
    }
}
