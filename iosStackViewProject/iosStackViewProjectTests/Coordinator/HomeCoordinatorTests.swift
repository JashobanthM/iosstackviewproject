//
//  HomeCoordinatorTests.swift
//  iosStackViewProjectTests
//
//  Created by Jashobanth Mohanty on 5/31/20.
//  Copyright © 2020 Jashobanth Mohanty. All rights reserved.
//

import XCTest

@testable import iosStackViewProject

class HomeCoordinatorTests: XCTestCase {
    var subject: HomeCoordinator!
    let mockRouter = MockRouter()

    override func setUp() {
        subject = HomeCoordinator(router: mockRouter)
    }

    func testStart() {
        XCTAssertNil(mockRouter.pushedViewController)
        subject.start()
        XCTAssertNotNil(mockRouter.pushedViewController)
    }
}
