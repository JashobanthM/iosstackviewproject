//
//  UsersViewControllerTest.swift
//  iosStackViewProjectTests
//
//  Created by Jashobanth Mohanty on 5/31/20.
//  Copyright © 2020 Jashobanth Mohanty. All rights reserved.
//

import XCTest

@testable import iosStackViewProject

class UsersViewControllerTest: XCTestCase {
    var subject: UsersViewController!
    var mockUsersViewModel = MockUserViewModel()

    override func setUp() {
       subject = UsersViewController(userViewModel: mockUsersViewModel)
    }

    func testFetchData() {
        XCTAssertFalse(self.mockUsersViewModel.getUsersCalled)
        subject.viewDidLoad()
        XCTAssertTrue(self.mockUsersViewModel.getUsersCalled)
    }
}
