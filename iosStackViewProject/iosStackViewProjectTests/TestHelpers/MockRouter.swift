//
//  MockRouter.swift
//  iosStackViewProjectTests
//
//  Created by Jashobanth Mohanty on 5/31/20.
//  Copyright © 2020 Jashobanth Mohanty. All rights reserved.
//

import UIKit
import Foundation
import iosStackViewProject

public class MockRouter: Routable {
    public var rootViewController: UIViewController? = nil
    public var topViewController: UIViewController? = nil

    public var presentedViewController: UIViewController? = nil
    public var pushedViewController: UIViewController? = nil

    public func present(viewController: UIViewController, animated: Bool) {
        presentedViewController = viewController
    }

    public func push(viewController: UIViewController, animated: Bool) {
        pushedViewController = viewController
    }
}
