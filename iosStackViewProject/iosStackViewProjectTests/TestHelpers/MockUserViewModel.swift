//
//  MockUserViewModel.swift
//  iosStackViewProjectTests
//
//  Created by Jashobanth Mohanty on 5/31/20.
//  Copyright © 2020 Jashobanth Mohanty. All rights reserved.
//

import iosStackViewProject

public class MockUserViewModel: UserVieweable {
    var getUsersCalled: Bool = false
    
    public init() {}
    
     public func getusers(completionHandler: @escaping (Result<[UserDetails], NetworkError>) -> Void) {
       getUsersCalled = true
    }
}
