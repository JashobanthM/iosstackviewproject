# iOSStackViewProject

Scalable solution using MVVM + Coordinators. Factory pattern was used to easily access colors/reusable views(in future can be used for fonts as well). 
Basic Unit testing.
#What this app contains
* Fetch details from https://api.stackexchange.com/2.2/users?site=stackoverflow 
* Display Username, badges earned if any, Avatar(profile pic)
* Mainly diplays MVVM+Coordinator pattern
* This app is developed using SnapKit for UI purpose. 
* Cocoapods needs to be installed. Inorder to install the pods we can use 
*  brew install cocoapods
*  pod install